# Lenguajes de programación y algoritmos
## -------------------------------------------------------------------------
# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightBlue
    }
    :depth(1) {
      BackGroundColor lightPink
    }
	:depth(2) {
      BackGroundColor Linen
    }
	:depth(3) {
      BackGroundColor lightCyan
    }
}
</style>
* Programando ordenadores en los 80 y ahora
** Antes
*** Ventajas
**** Menor cantidad de bugs
**** MEjor planificación
*** Desventajas
**** Recursos limitados
**** Tiempo de espera largo
*** Caracteristicas
**** Programacion de bajo nivel
**** CPUs mas rapidas
*** Lenguaje de bajo nivel
**** Ensamblador
***** Aprovecha los recursos
***** Control de ejecucion
**** Gran eficiencia
**** los programas son mejor planteados 
*** tiempo de ejecucion mas rapido
** Ahora
*** lenguaje de alto nivel
**** Uso de compiladores
**** C
***** Utiliza las librerias
**** java
***** Maquina virtual
**** C++
*** Problemas 
**** Tiempo de ejecucion menos rapido
**** Poca eficiencia
**** Bugs
*** Se trabaja en capas
*** Uso de interpretes
*** Mayor Potencia en Hz

@endmindmap
```

# Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml

@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightBlue
    }
    :depth(1) {
      BackGroundColor lightPink
    }
	:depth(2) {
      BackGroundColor Linen
    }
    :depth(3) {
      BackGroundColor Moccasin
    }
}
</style>
* Hª de los algoritmos y de los \n lenguajes de programación
** Algoritmos
*** No puede hacer procesos lógicos
*** Limites
**** cuanto tiempo va a tardar la ejecucion
**** Saber en que momento \n terminar una ejecucion
*** Permite hallar la solucion \n de un problema
**** Entrada
**** Desarrollo del sistema
**** Salida
**** Secuencia ordenada
*** No da lugares a ambiguedades
** Lenguajes de programacion
*** Primera generación
**** Lenguaje Maquina
*** Segunda generación
**** años 40s
***** Lenguaje ensamblador
*** Tercera generación
**** Lenguajes de alto nivel
***** FORTRAN
****** Fecha: 1957 
****** orientado a resolver formulas
***** COBOL 1960
****** De gestión
***** BASIC 1964
****** de facil aprendizaje
***** LOGO 1964
***** PASCAL 1970
*** Cuarta generación
**** C 1972
**** C++ 1979 
**** HTML, PYTHON
**** Java, Javascrip, PHP
***** Orientado a Objetos
***** Orientado a objetos
*** Mas utilizados
**** c
**** java
**** c++
*** Familia de lenguajes o paradigmas
**** Lisp
**** Logica
**** Programacion orientada a objetos
**** Imperativo
@endmindmap
```
# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightBlue
    }
    :depth(1) {
      BackGroundColor lightPink
    }
	:depth(2) {
      BackGroundColor Linen
    }
	:depth(3) {
      BackGroundColor lightCyan
    }
    :depth(4) {
      BackGroundColor Moccasin
    }
}
</style>
* Tendencias actuales en los lenguajes de \n  Programación y Sistemas Informáticos
** Lenguajes de Programacion
*** El uso de Lenguaje ensamblador
*** Programacion Orientada a Objetos
**** Todos los aspectos son separados
**** Organiza el diseño de software
**** Se define como campo de datos
*** Programacion concurrente
**** Se usan para expresar el paralelismo entre tareas
**** Sincronización entre procesos
*** Se adapta segun las necesidades
*** Sirve para la comunicaciion entre humano-maquina
** La evolucion de los lenguajes y \n paradigmas de programacion
*** Programacion estructurada
**** Separa la dependencia del haedware
**** Fallos en el paradigma al programar
**** Permite pensar como descomponer la solucion
**** Tipos de lenguajes como: 
***** C 
***** Basic
***** Pascal
*** Logico
**** Los resultados solo son:
***** True 
***** False
**** Lenguaje de la logica
**** Recursividad
*** Programacion funcional
**** Se basa en funciones matemáticas
**** Una herramienta importante como la recursividad
**** Lenguajes como Haskell y ML
** Familia de lenguajes o paradigmas
*** Multiples usuarios puedan acceder al sistema
*** Programacion Orientada a Objetos
**** Analiza una funcion principal
**** Nueva manera de organizacion\n del programa
**** Cambia las nececidades
*** Paradigma funcional
**** Esta basada en la recursividad
**** Lenguajes que lo usan
***** Haskel
***** Hope
***** MI
*** Paradigma lógico
**** Lenguajes que lo usan
***** Prolog
***** Mercury
***** CLP
*** Distribuida
**** Se puede tener en varios ordenadores
*** Programacion Orientada a Aspectos
**** Agrega aspectos como una capa
**** Integrarse en un proyecto juntos
			
@endmindmap
```
